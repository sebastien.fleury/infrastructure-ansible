# Inventaire
2. Je crée mon inventaire avec les IP de mes serveurs
3. Je teste mo inventaire avec la commande `ansible -i inventory.yml all -m ping -k`


# Playbook systeme
2. déployer un utilisateur `sysadmin` faisant parti du groupe `admin`
> module user 
2. Installer les packages `htop`  + `tree`
> module package
3. déployer votre clés ssh aux utilisateurs sysadmin et esiee
> module authorized_key (https://docs.ansible.com/ansible/latest/collections/ansible/posix/authorized_key_module.html)
4. organiser quelques variables dans votre playbook
5. Executer le playbook avec -K pour le password sudo
> ansible-playbook -i inventory.yml playbook-system.yml -k -K

# TP web
* Utiliser un role existant (geerlingguy.apache)
* utiliser les variables de ce role pour déployer un site web

# TP Database
* Utiliser un role existant (geerlingguy.mysql)
* utiliser les variables de ce role pour déployer un site web
* Déployer un serveur BDD avec une BDD `esiee` et un user dédié `esiee`

# TP Application
* Déployer le logiciel GLPI sur le serveur Web avec sa connexion à la base de données
